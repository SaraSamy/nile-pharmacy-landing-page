import 'package:flutter/material.dart';

class AppTheme {

  static const TextStyle aboutL= TextStyle(
    fontFamily: 'Gayathri',
    color: Colors.white,
    fontSize: 34,
    wordSpacing: 0.4,
    letterSpacing: 0.2,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle aboutS= TextStyle(
    fontFamily: 'Gayathri',
    color: Colors.white,
    fontSize: 22,
    wordSpacing: 0.2,
    letterSpacing: 0.1,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle aboutM= TextStyle(
    fontFamily: 'Gayathri',
    color: Colors.white,
    fontSize: 28,
    wordSpacing: 0.2,
    letterSpacing: 0.1,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle featureDiscL= TextStyle(
    fontFamily: 'Ubuntu',
    color: const Color(0xff6285f3),
    fontSize: 28,
    wordSpacing: 0.2,
    letterSpacing: 0.2,
  );

  static const TextStyle featureTitleL= TextStyle(
    fontFamily: 'Gayathri',
    color: const Color(0xffe5485a),
    fontSize: 42,
    letterSpacing: 1.0,
    fontWeight: FontWeight.w900,
  );

  static const TextStyle featureDiscM= TextStyle(
    fontFamily: 'Ubuntu',
    color: const Color(0xff6285f3),
    fontSize: 24,
    wordSpacing: 0.2,
    letterSpacing: 0.2,
  );

  static const TextStyle featureTitleM= TextStyle(
    fontFamily: 'Gayathri',
    color: const Color(0xffe5485a),
    fontSize: 30,
    letterSpacing: 1.0,
    fontWeight: FontWeight.w900,
  );

  static const TextStyle featureDiscS= TextStyle(
    fontFamily: 'Ubuntu',
    color: const Color(0xff6285f3),
    fontSize: 14,
    wordSpacing: 0.1,
    letterSpacing: 0.1,
  );

  static const TextStyle featureTitleS= TextStyle(
    fontFamily: 'Gayathri',
    color: const Color(0xffe5485a),
    fontSize: 20,
    letterSpacing: 1.0,
    fontWeight: FontWeight.w900,
  );

  static const TextStyle downloadL= TextStyle(
    fontFamily: 'Ubuntu',
//    color: const Color(0xffe5485a),
  color: Colors.white,
    fontSize: 26,
    letterSpacing: 1.0,
    wordSpacing: 0.5,
    fontWeight: FontWeight.bold,
  );

  static const TextStyle downloadDiscL= TextStyle(
    fontFamily: 'Ubuntu',
    //    color: const Color(0xffe5485a),
    color: Colors.white,
    fontSize: 20,
    wordSpacing: 0.5,
    letterSpacing: 0.5,
  );

  static const TextStyle downloadM= TextStyle(
    fontFamily: 'Ubuntu',
//    color: const Color(0xffe5485a),
  color: Colors.white,
    fontSize: 20,
    letterSpacing: 1.0,
    wordSpacing: 0.5,
    fontWeight: FontWeight.bold,
  );

  static const TextStyle downloadDiscM= TextStyle(
    fontFamily: 'Ubuntu',
    //    color: const Color(0xffe5485a),
    color: Colors.white,
    fontSize: 18,
    wordSpacing: 0.5,
    letterSpacing: 0.5,
  );
  static const TextStyle downloadS= TextStyle(
    fontFamily: 'Ubuntu',
//    color: const Color(0xffe5485a),
  color: Colors.white,
    fontSize: 16,
    letterSpacing: 0.1,
    wordSpacing: 0.5,
    fontWeight: FontWeight.bold,
  );

  static const TextStyle downloadDiscS= TextStyle(
    fontFamily: 'Ubuntu',
    //    color: const Color(0xffe5485a),
    color: Colors.white,
    fontSize: 14,
    wordSpacing: 0.5,
    letterSpacing: 0.5,
  );

}
