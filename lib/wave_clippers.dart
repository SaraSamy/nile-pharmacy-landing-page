import 'package:flutter/widgets.dart';

class WaveClipper1 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width *0.3, 0.0);

    path.quadraticBezierTo(size.width * 0.45, size.height * .25, size.width * .3, size.height * 0.5);

    path.quadraticBezierTo(size.width * 0.55, size.height * 0.75, size.width * 0.3, size.height);

    path.lineTo(size.width * 0.7, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

/// 波浪2
class WaveClipper2 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width * 0.98, 0.0);
    path.quadraticBezierTo(size.width * 0.4, size.height * .25, size.width * .8, size.height * 0.5);

    path.quadraticBezierTo(size.width * 0.98, size.height * 0.75, size.width * 0.45, size.height);

    path.lineTo(size.width * 0.2, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

/// 波浪3
class WaveClipper3 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0.0,size.height * 0.72);
    path.quadraticBezierTo(size.width * 0.1, size.height * .99, size.width * .28,  size.height);
    path.lineTo(0.0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
