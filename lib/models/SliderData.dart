import 'package:flutter/material.dart';

class SliderData {
  final String title;
  final String imagePath;
  final String description;

  SliderData({this.title, this.imagePath, this.description});
}
