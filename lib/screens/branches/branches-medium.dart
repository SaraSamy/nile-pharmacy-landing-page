// ignore: avoid_web_libraries_in_flutter
import 'dart:html';
import 'dart:ui' as ui;
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_maps/google_maps.dart' hide Icon;

class BranchesMedium extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        ConstrainedBox(
          child: Image.asset(
            "assets/images/login_cover.jpg",
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.fitWidth,
            height: 525,
          ),
          constraints: BoxConstraints.expand(
            height: 525,
          ),
        ),
        Positioned(
          child: Container(
            height: 525,
            child: ClipRRect(
              // make sure we apply clip it properly
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5, sigmaY: 7),
                child: Container(
                  alignment: Alignment.center,
                  color: Colors.blue.withOpacity(0.3),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        color: Colors.white,
                        height: 375,
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width * 0.3,
                              height: 250,
                              child: getMap(),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Container(
                              child: Text(
                                " فرع الكورنيش ",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.28,
//                            padding: EdgeInsets.symmetric(horizontal: 4.0),
                              child: Text(
                                "Fekry Zaher, Qism Damietta, Damietta, Damietta Governorate",
                                style: TextStyle(
                                    fontSize: 16.0, color: Colors.black54),
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                  MediaQuery.of(context).size.width * 0.01),
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: Text(
                                  "057 2324855",
                                  style: TextStyle(
                                      fontSize: 14.0, color: Colors.blue),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        height: 375,
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width * 0.3,
                              height: 250,
                              child: getMap(),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Container(
                              child: Text(
                                "فرع الميناء ",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.28,
                              child: Text(
                                "center, Damietta Port Rd, Damietta El-Gadeeda City, Kafr Saad, Damietta Governorate 34511",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black54),
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                  MediaQuery.of(context).size.width * 0.01),
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: Text(
                                  "0109 994 6666",
                                  style: TextStyle(
                                      fontSize: 14.0, color: Colors.blue),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        height: 375,
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width * 0.3,
                              height: 250,
                              child: getMap(),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Container(
                              child: Text(
                                " فرع الحربي ",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.28,
                              child: Text(
                                "Al- harby, Shatt Izbat Al Lahm, Damietta, Damietta Governorate",
                                style: TextStyle(
                                    fontSize: 14.0, color: Colors.black54),
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.01),
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: Text(
                                  "057 2330880",
                                  style: TextStyle(fontSize: 14.0, color: Colors.blue),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          top: 18.0,
          left: 0,
          right: 0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Our branches',
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Ubuntu',
                    fontSize: 32.0,
                    letterSpacing: 0.5),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

Widget getMap() {
  String htmlId = "7";

  // ignore: undefined_prefixed_name
  ui.platformViewRegistry.registerViewFactory(htmlId, (int viewId) {
    final myLatlng = new LatLng(30.2669444, -97.7427778);

    final mapOptions = new MapOptions()
      ..zoom = 8
      ..center = new LatLng(30.2669444, -97.7427778);

    final elem = DivElement()
      ..id = htmlId
      ..style.width = "100%"
      ..style.height = "100%"
      ..style.border = 'none';

    final map = new GMap(elem, mapOptions);

    Marker(MarkerOptions()
      ..position = myLatlng
      ..map = map
      ..title = 'Hello World!');

    return elem;
  });

  return HtmlElementView(viewType: htmlId);
}
