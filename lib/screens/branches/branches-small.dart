// ignore: avoid_web_libraries_in_flutter
import 'dart:html';
import 'dart:ui' as ui;
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_maps/google_maps.dart' hide Icon;

class BranchesSmall extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(bottom: 4.0),
      color: Colors.blue.withOpacity(0.3),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(
            height: 8,
          ),
          Container(
            padding: EdgeInsets.only(top: 4.0),
            child: Text(
              'Our branches',
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Ubuntu',
                  fontSize: 18.0,
                  fontWeight: FontWeight.w900,
                  letterSpacing: 0.5),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Container(
            color: Colors.white,
            width: MediaQuery.of(context).size.width * 0.95,
            padding: EdgeInsets.all(4.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.95,
                  height: 200,
                  child: getMap(),
                ),
                SizedBox(
                  height: 4,
                ),
                Container(
                  child: Text(
                    " فرع الكورنيش ",
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.85,
//                            padding: EdgeInsets.symmetric(horizontal: 4.0),
                  child: Text(
                    "Fekry Zaher, Qism Damietta, Damietta, Damietta Governorate",
                    style: TextStyle(
                        fontSize: 16.0, color: Colors.black54),
                  ),
                ),
                SizedBox(
                  height:4,
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                      horizontal:
                      MediaQuery.of(context).size.width * 0.01),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: Text(
                      "057 2324855",
                      style: TextStyle(
                          fontSize: 14.0, color: Colors.blue),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 4,
          ),
          Container(
            color: Colors.white,
            width: MediaQuery.of(context).size.width * 0.95,
            padding: EdgeInsets.all(4.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.95,
                  height: 200,
                  child: getMap(),
                ),
                SizedBox(
                  height: 4,
                ),
                Container(
                  child: Text(
                    "فرع الميناء ",
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.85,
                  child: Text(
                    "center, Damietta Port Rd, Damietta El-Gadeeda City, Kafr Saad, Damietta Governorate 34511",
                    style: TextStyle(
                        fontSize: 14.0, color: Colors.black54),
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                      horizontal:
                      MediaQuery.of(context).size.width * 0.01),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: Text(
                      "0109 994 6666",
                      style: TextStyle(
                          fontSize: 14.0, color: Colors.blue),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 4,
          ),
          Container(
            color: Colors.white,
            width: MediaQuery.of(context).size.width * 0.95,
            padding: EdgeInsets.all(4.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.95,
                  height: 200,
                  child: getMap(),
                ),
                SizedBox(
                  height: 4,
                ),
                Container(
                  child: Text(
                    " فرع الحربي ",
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.85,
                  child: Text(
                    "Al- harby, Shatt Izbat Al Lahm, Damietta, Damietta Governorate",
                    style: TextStyle(
                        fontSize: 14.0, color: Colors.black54),
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.01),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: Text(
                      "057 2330880",
                      style: TextStyle(fontSize: 14.0, color: Colors.blue),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 8,
          ),
        ],
      ),
    );
  }
}

Widget getMap() {
  String htmlId = "7";

  // ignore: undefined_prefixed_name
  ui.platformViewRegistry.registerViewFactory(htmlId, (int viewId) {
    final myLatlng = new LatLng(30.2669444, -97.7427778);

    final mapOptions = new MapOptions()
      ..zoom = 8
      ..center = new LatLng(30.2669444, -97.7427778);

    final elem = DivElement()
      ..id = htmlId
      ..style.width = "100%"
      ..style.height = "100%"
      ..style.border = 'none';

    final map = new GMap(elem, mapOptions);

    Marker(MarkerOptions()
      ..position = myLatlng
      ..map = map
      ..title = 'Hello World!');

    return elem;
  });

  return HtmlElementView(viewType: htmlId);
}
