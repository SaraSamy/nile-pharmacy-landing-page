import 'package:flutter/material.dart';
import 'dart:js' as js;
import '../../styleguide.dart';

class FooderLarge extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
//      color: Colors.white70,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            const Color(0xff6285f3),
            const Color(0xffeffdfe),
          ],
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            "assets/images/download.png",
            width: MediaQuery.of(context).size.width * 0.25,
          ),
          SizedBox(width: MediaQuery.of(context).size.width * 0.1),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "DOWNLOAD OUR APP",
                style: AppTheme.downloadL,
              ),
              Container(
                margin: EdgeInsets.only(top: 4.0),
                width: MediaQuery.of(context).size.width * 0.32,
                child: Text(
                  "Join thousands of users ordering their medications from the comfort of their phones.",
                  style: AppTheme.downloadDiscL,
                ),
              ),
              SizedBox(
                height: 1,
              ),
              Row(
                children: <Widget>[
                  InkWell(
                    onTap: (){

                    },
                    child: Image.asset(
                      "assets/images/apple.png",
                      width: MediaQuery.of(context).size.width * 0.1,
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.01),
                  InkWell(
                    onTap: (){
                      js.context.callMethod("open", ["https://play.google.com/store/apps/details?id=hamza.solutions.saidlty"]);
                    },
                    child: Image.asset(
                      "assets/images/google.png",
                      width: MediaQuery.of(context).size.width * 0.1,
                    ),
                  ),
                ],
              ),
              Text(
                "FOLLOW US",
                style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 0.5),
              ),
              SizedBox(
                height: 4,
              ),
              Row(
                children: <Widget>[
                  InkWell(
                    onTap: (){
                      js.context.callMethod("open", ["https://www.facebook.com/Nilepharmacy/"]);
                    },
                    child: Image.asset(
                      "assets/images/facebook.png",
                      width: MediaQuery.of(context).size.width * 0.015,
                    ),
                  ),
                  SizedBox(width: 4),
                  InkWell(
                    onTap: (){

                    },
                    child: Image.asset(
                      "assets/images/twitter.png",
                      width: MediaQuery.of(context).size.width * 0.015,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
