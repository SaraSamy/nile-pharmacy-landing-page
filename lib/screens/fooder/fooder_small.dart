import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'dart:js' as js;
import 'package:nile_pharmacy/models/SliderData.dart';
import '../../styleguide.dart';
import '../../wave_clippers.dart';

class FooderSmall extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
//      color: Colors.white70,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            const Color(0xff6285f3),
            const Color(0xffeffdfe),
          ],
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 28),
          Image.asset(
            "assets/images/download.png",
            height:200 ,
            width: MediaQuery.of(context).size.width * 0.75,
          ),
          SizedBox(height: 18),
          Text(
            "DOWNLOAD OUR APP",
            style: AppTheme.downloadL,
          ),
          Container(
            margin: EdgeInsets.only(top: 8.0),
            width: MediaQuery.of(context).size.width * 0.9,
            child: Text(
              "Join thousands of users ordering their medications from the comfort of their phones.",
              style: AppTheme.downloadDiscL,
            ),
          ),
          SizedBox(
            height: 4,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InkWell(
                onTap: (){

                },
                child: Image.asset(
                  "assets/images/apple.png",
                  width: MediaQuery.of(context).size.width * 0.35,
                ),
              ),
              SizedBox(width: MediaQuery.of(context).size.width * 0.01),
              InkWell(
                onTap: (){
                  js.context.callMethod("open", ["https://play.google.com/store/apps/details?id=hamza.solutions.saidlty"]);
                },
                child: Image.asset(
                  "assets/images/google.png",
                  width: MediaQuery.of(context).size.width * 0.35,
                ),
              ),
            ],
          ),
          Text(
            "FOLLOW US",
            style: TextStyle(
                fontSize: 16.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5),
          ),
          SizedBox(
            height: 4,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InkWell(
                onTap: (){
                  js.context.callMethod("open", ["https://www.facebook.com/Nilepharmacy/"]);
                },
                child: Image.asset(
                  "assets/images/facebook.png",
                  width: MediaQuery.of(context).size.width * 0.05,
                ),
              ),
              SizedBox(width: 4),
              InkWell(
                onTap: (){

                },
                child: Image.asset(
                  "assets/images/twitter.png",
                  width: MediaQuery.of(context).size.width * 0.05,
                ),
              ),
            ],
          ),

          SizedBox(
            height: 28,
          ),
        ],
      ),
    );
  }
}
