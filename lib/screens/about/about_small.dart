import 'package:clippy_flutter/clippy_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nile_pharmacy/styleguide.dart';

class AboutSmall extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
          width: MediaQuery.of(context).size.width,
          height: 550,
          child: Stack(
            children: <Widget>[
              Diagonal(
                position: DiagonalPosition.BOTTOM_LEFT,
                clipShadows: [ClipShadow(color: Colors.white)],
                clipHeight: 70,
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.6,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        const Color(0xff3c67f1),
                        const Color(0xffb2f9fc),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 8,
                top: 20,
                child: Container(
                  child: Wrap(
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          new Container(
                            margin: const EdgeInsets.only(right: 4.0),
                            child: Image.asset(
                              "assets/images/whatsapp.png",
                              fit: BoxFit.fitWidth,
                              width: 16,
                            ),
                          ),
                          Text(
                            "0109 994 6666",
                            style: TextStyle(
                                fontSize: 16.0, color: Colors.white),
                          )
                        ],
                      ),
                      SizedBox(width: MediaQuery.of(context).size.width * 0.04),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          new Container(
                            margin: const EdgeInsets.only(right: 4.0),
                            child: Image.asset(
                              "assets/images/hotline.png",
                              fit: BoxFit.fitWidth,
                              width: 18,
                            ),
                          ),
//
                          Text(
                            "15077",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                right: 8.0,
                top: 8.0,
                child: Image.asset(
                  "assets/images/logo.png",
                  width: 75,
                  height: 50,
                  fit: BoxFit.cover,
                ),
              ),
              Positioned(
                top: 75,
                left: 2,
                right: 2,
                child: Column(
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: Text(
                          "We facilitate connection between you and our pharmacy, provide you by the latest offers, products, news.",
                          style: AppTheme.aboutS,
                        )),
                    Image.asset(
                      "assets/images/app.png",
                      height: 300,
                    ),
                  ],
                ),
              ),
            ],
          ))
    ;
  }
}
