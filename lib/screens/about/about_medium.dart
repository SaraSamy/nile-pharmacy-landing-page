import 'package:clippy_flutter/clippy_flutter.dart';
import 'package:flutter/material.dart';
import 'package:nile_pharmacy/styleguide.dart';

class AboutMedium extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.8,
        child: Stack(
          children: <Widget>[
            Diagonal(
              position: DiagonalPosition.BOTTOM_LEFT,
              clipShadows: [ClipShadow(color: Colors.white)],
              clipHeight: 50,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.75,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      const Color(0xff3c67f1),
                      const Color(0xffb2f9fc),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              left: 8,
              top: 38,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.4,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          new Container(
                            margin: const EdgeInsets.only(right: 8.0),
                            child: Image.asset(
                              "assets/images/whatsapp.png",
                              fit: BoxFit.fitWidth,
                              width: 30,
                            ),
                          ),
                          Text(
                            "0109 994 6666",
                            style:
                            TextStyle(fontSize: 18.0, color: Colors.white),
                          )
                        ],
                      ),
                    ),
                    SizedBox(width: MediaQuery.of(context).size.width * 0.04),
                    Center(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          new Container(
                            margin: const EdgeInsets.only(right: 8.0),
                            child: Image.asset(
                              "assets/images/hotline.png",
                              fit: BoxFit.fitWidth,
                              width: 32,
                            ),
                          ),
//
                          Text(
                            "15077",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              right: 48.0,
              top: 8.0,
              child: Image.asset(
                "assets/images/logo.png",
                width: 115,
                height: 75,
                fit: BoxFit.cover,
              ),
            ),
            Positioned(
              top: 100,
              left: 0.0,
              right: 0.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: new Container(
                      width: MediaQuery.of(context).size.width * 0.4,
                      child: Text(
                        "We facilitate connection between you and our pharmacy, provide you by the latest offers, products, news.",
                        style: AppTheme.aboutM,
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.06),
                  Image.asset(
                    "assets/images/app.png",
                    height: MediaQuery.of(context).size.height * 0.50,
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
