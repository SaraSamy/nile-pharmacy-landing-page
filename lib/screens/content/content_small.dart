import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_multi_carousel/carousel.dart';
import 'package:nile_pharmacy/models/SliderData.dart';
import '../../styleguide.dart';
import '../../wave_clippers.dart';


class ContentSmall extends StatelessWidget {
  List<SliderData> sliderItems = [
    SliderData(
        title: "We provide",
        imagePath: "assets/images/pharmacy.png",
        description:
        "Browse and select medicine from our online medical store immediately, or Simply send a picture of your prescription and we will get it for you."),
    SliderData(
        title: "Live chat",
        imagePath: "assets/images/doctor.png",
        description:
        "We gives you on-demand access to an online pharmacist via a secure chat. All our pharmacists are registered with the Medical Council."),
    SliderData(
        title: "Q&A",
        imagePath: "assets/images/qanda.png",
        description:
        "Now you can easily ask medical questions and receive answers from our professional pharmacists."),
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 12.0),
        child: CarouselSlider(
          enableInfiniteScroll: true,
          autoPlay: true,
          autoPlayInterval: Duration(seconds: 3),
          autoPlayAnimationDuration: Duration(milliseconds: 1000),
          autoPlayCurve: Curves.fastOutSlowIn,
          pauseAutoPlayOnTouch: Duration(seconds: 10),
          enlargeCenterPage: true,
          scrollDirection: Axis.horizontal,
          items: sliderItems.map((index) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                  width: MediaQuery.of(context).size.width ,
                  margin: EdgeInsets.symmetric(horizontal: 2.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: const Color(0xffeffdfe)),
                  ),
                  child: Stack(
                    children: <Widget>[
                      ClipPath(
                        clipper: WaveClipper2(),
                        child: Container(
                          width: double.infinity,
                          height: double.infinity,
//                                    color: const Color(0xffeffdfe)
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                                const Color(0xff6285f3),
                                const Color(0xffeffdfe),
                              ])),
                        ),
                      ),
                      Positioned(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  index.title,
                                  style: AppTheme.featureTitleS,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 4.0),
                                  width: MediaQuery.of(context).size.width * 0.4,
                                  child: Text(
                                    index.description,
                                    style: AppTheme.featureDiscS,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                                width:4),
                            Image.asset(
                              index.imagePath,
                              width: MediaQuery.of(context).size.width * 0.3,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          }).toList(),
        )
    );
//      Container(
//        margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
//      child: Carousel(
//          height: 300.0,
//          width: MediaQuery.of(context).size.width,
//          type: "simple",
//          indicatorType: "dot",
//          activeIndicatorColor:Colors.blue,
//          arrowColor: Colors.blue,
//          axis: Axis.horizontal,
//          showArrow: false,
//          indicatorBackgroundOpacity:0.0,
//          children: List.generate(
//              sliderItems.length,
//                  (i) => Center(
//                child: Container(
//                  width: MediaQuery.of(context).size.width,
//                  margin: EdgeInsets.symmetric(horizontal: 2.0),
//                  decoration: BoxDecoration(
//                    border: Border.all(color: const Color(0xffeffdfe)),
//                  ),
//                  child: Stack(
//                    children: <Widget>[
//                      ClipPath(
//                        clipper: WaveClipper2(),
//                        child: Container(
//                          width: double.infinity,
//                          height: double.infinity,
////                                    color: const Color(0xffeffdfe)
//                          decoration: BoxDecoration(
//                              gradient: LinearGradient(colors: [
//                                const Color(0xff6285f3),
//                                const Color(0xffeffdfe),
//                              ])),
//                        ),
//                      ),
//                      Positioned(
//                        top: 8,
//                        bottom: 8,
//                        left: 8,
//                        right: 8,
//                        child: Row(
//                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                          children: <Widget>[
//                            Column(
//                              mainAxisAlignment: MainAxisAlignment.center,
//                              children: <Widget>[
//                                Text(
//                                  sliderItems[i].title,
//                                  style: AppTheme.featureTitleS,
//                                ),
//                                Container(
//                                  width: MediaQuery.of(context).size.width * 0.5,
//                                  child: Text(
//                                    sliderItems[i].description,
//                                    style: AppTheme.featureDiscS,
//                                  ),
//                                ),
//                              ],
//                            ),
//                            Image.asset(
//                              sliderItems[i].imagePath,
//                              width: MediaQuery.of(context).size.width * 0.4,
//                            ),
//                          ],
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ))
//    ),
////        child: CarouselSlider(
////          height: 350,
////          enableInfiniteScroll: true,
////          autoPlay: true,
////          autoPlayInterval: Duration(seconds: 3),
////          autoPlayAnimationDuration: Duration(milliseconds: 1000),
////          autoPlayCurve: Curves.fastOutSlowIn,
////          pauseAutoPlayOnTouch: Duration(seconds: 10),
////          enlargeCenterPage: true,
////          scrollDirection: Axis.horizontal,
////          items: sliderItems.map((index) {
////            return Builder(
////              builder: (BuildContext context) {
////                return Container(
////                  width: MediaQuery.of(context).size.width,
////                  margin: EdgeInsets.symmetric(horizontal: 2.0),
////                  decoration: BoxDecoration(
////                    border: Border.all(color: const Color(0xffeffdfe)),
////                  ),
////                  child: Stack(
////                    children: <Widget>[
////                      ClipPath(
////                        clipper: WaveClipper2(),
////                        child: Container(
////                          width: double.infinity,
////                          height: double.infinity,
//////                                    color: const Color(0xffeffdfe)
////                          decoration: BoxDecoration(
////                              gradient: LinearGradient(colors: [
////                                const Color(0xff6285f3),
////                                const Color(0xffeffdfe),
////                              ])),
////                        ),
////                      ),
////                      Positioned(
////                        top: 8,
////                        bottom: 8,
////                        left: 8,
////                        right: 8,
////                        child: Row(
////                          mainAxisAlignment: MainAxisAlignment.center,
////                          children: <Widget>[
////                            Column(
////                              mainAxisAlignment: MainAxisAlignment.center,
////                              children: <Widget>[
////                                Text(
////                                  index.title,
////                                  style: AppTheme.featureTitleM,
////                                ),
////                                Container(
////                                  margin: EdgeInsets.only(top: 2.0),
////                                  width:
////                                  MediaQuery.of(context).size.width * 0.44,
////                                  child: Text(
////                                    index.description,
////                                    style: AppTheme.featureDiscM,
////                                  ),
////                                ),
////                              ],
////                            ),
////                            SizedBox(
////                                width:
////                                MediaQuery.of(context).size.width * 0.001),
////                            Image.asset(
////                              index.imagePath,
////                              width: MediaQuery.of(context).size.width * 0.31,
////                            ),
////                          ],
////                        ),
////                      ),
////                    ],
////                  ),
////                );
////              },
////            );
////          }).toList(),
////        )
//    );
  }
}
