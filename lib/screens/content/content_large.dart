import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:nile_pharmacy/models/SliderData.dart';
import '../../styleguide.dart';
import '../../wave_clippers.dart';

class ContentLarge extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<SliderData> sliderItems = [
      SliderData(
          title: "We provide",
          imagePath: "assets/images/pharmacy.png",
          description:
              "Browse and select medicine from our online medical store immediately, or Simply send a picture of your prescription and we will get it for you."),
      SliderData(
          title: "Live chat",
          imagePath: "assets/images/doctor.png",
          description:
              "We gives you on-demand access to an online pharmacist via a secure chat. All our pharmacists are registered with the Medical Council."),
      SliderData(
          title: "Q&A",
          imagePath: "assets/images/qanda.png",
          description:
              "Now you can easily ask medical questions and receive answers from our professional pharmacists."),
    ];
    return Container(
        margin: EdgeInsets.only(top: 8.0, left: 18.0, right: 18.0),
        child: CarouselSlider(
          height: 500,
          enableInfiniteScroll: true,
          autoPlay: true,
          autoPlayInterval: Duration(seconds: 3),
          autoPlayAnimationDuration: Duration(milliseconds: 1000),
          autoPlayCurve: Curves.fastOutSlowIn,
          pauseAutoPlayOnTouch: Duration(seconds: 10),
          enlargeCenterPage: true,
          scrollDirection: Axis.horizontal,
          items: sliderItems.map((index) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                  width: MediaQuery.of(context).size.width * 0.80,
                  margin: EdgeInsets.symmetric(horizontal: 4.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: const Color(0xffeffdfe)),
                  ),
                  child: Stack(
                    children: <Widget>[
                      ClipPath(
                        clipper: WaveClipper2(),
                        child: Container(
                          width: double.infinity,
                          height: double.infinity,
//                                    color: const Color(0xffeffdfe)
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                            const Color(0xff6285f3),
                            const Color(0xffeffdfe),
                          ])),
                        ),
                      ),
                      Positioned(
                        top: 8,
                        bottom: 8,
                        left: 8,
                        right: 8,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  index.title,
                                  style: AppTheme.featureTitleL,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 16.0),
                                  width:
                                      MediaQuery.of(context).size.width * 0.28,
                                  child: Text(
                                    index.description,
                                    style: AppTheme.featureDiscL,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                                width:
                                    MediaQuery.of(context).size.width * 0.01),
                            Image.asset(
                              index.imagePath,
                              width: MediaQuery.of(context).size.width * 0.38,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          }).toList(),
        ));
  }
}
