import 'package:flutter/material.dart';
import 'package:nile_pharmacy/screens/about/about_large.dart';
import 'package:nile_pharmacy/screens/about/about_medium.dart';
import 'package:nile_pharmacy/screens/about/about_small.dart';
import 'package:nile_pharmacy/screens/branches/branches-large.dart';
import 'package:nile_pharmacy/screens/branches/branches-medium.dart';
import 'package:nile_pharmacy/screens/branches/branches-small.dart';
import 'package:nile_pharmacy/screens/content/content_large.dart';
import 'package:nile_pharmacy/screens/content/content_medium.dart';
import 'package:nile_pharmacy/screens/content/content_small.dart';
import 'package:nile_pharmacy/screens/fooder/fooder-large.dart';
import 'package:nile_pharmacy/screens/fooder/fooder_medium.dart';
import 'package:nile_pharmacy/screens/fooder/fooder_small.dart';
import 'package:nile_pharmacy/utils/responsiveLayout.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Nile pharmacy',
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Colors.white,
          body:   Body(),
        ));
  }
}

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ResponsiveLayout(
      largeScreen: LargeChild(),
      smallScreen: SmallChild(),
      mediumScreen: MediumChild(),
    );
  }
}

class LargeChild extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        AboutLarge(),
        SizedBox(height: 8),
        SizedBox(
          child: new Center(
            child: new Container(
              margin: new EdgeInsetsDirectional.only(start: 50.0, end: 50.0),
              height: 1.0,
              color: Colors.black12,
            ),
          ),
        ),
        SizedBox(height: 8),
        ContentLarge(),
        SizedBox(height: 28),
        SizedBox(
          child: new Center(
            child: new Container(
              margin: new EdgeInsetsDirectional.only(start: 50.0, end: 50.0),
              height: 1.0,
              color: Colors.black12,
            ),
          ),
        ),
        SizedBox(height: 28),
        BranchesLarge(),
        FooderLarge()
      ],
    );
  }
}

class MediumChild extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        AboutMedium(),
        SizedBox(height: 8),
        SizedBox(
          child: new Center(
            child: new Container(
              margin: new EdgeInsetsDirectional.only(start: 50.0, end: 50.0),
              height: 1.0,
              color: Colors.black12,
            ),
          ),
        ),
        SizedBox(height: 8),
        ContentMedium(),
        SizedBox(height: 28),
        SizedBox(
          child: new Center(
            child: new Container(
              margin: new EdgeInsetsDirectional.only(start: 50.0, end: 50.0),
              height: 1.0,
              color: Colors.black12,
            ),
          ),
        ),
        SizedBox(height: 28),
        BranchesMedium(),
        FooderMedium()
      ],
    );
  }
}

class SmallChild extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        AboutSmall(),
        SizedBox(height: 4),
        SizedBox(
          child: new Center(
            child: new Container(
              margin: new EdgeInsetsDirectional.only(start: 50.0, end: 50.0),
              height: 1.0,
              color: Colors.black12,
            ),
          ),
        ),
        SizedBox(height: 4),
        ContentSmall(),
        SizedBox(height: 4),
        SizedBox(
          child: new Center(
            child: new Container(
              margin: new EdgeInsetsDirectional.only(start: 50.0, end: 50.0),
              height: 1.0,
              color: Colors.black12,
            ),
          ),
        ),
        SizedBox(height: 4),
        BranchesSmall(),
        FooderSmall()
      ],
    );
  }
}
